#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/list.h>
#include <linux/sched.h>
#include <linux/mutex.h>
#include <linux/ioctl.h>
#include <linux/version.h>


#define LICENCE "GPL"
#define AUTEUR "Michael Adalbert michael.adalbert@univ-tlse3.fr"
#define DESCRIPTION "Exemple de module Master CAMSI"
#define DEVICE "device_tp6"

#define MIN(a, b) ((a) < (b)) ? (a) : (b)

static int my_open(struct inode *i, struct file *f);
static int my_release(struct inode *i, struct file *f);
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
static int my_ioctl(struct inode *i, struct file *f, unsigned int cmd, unsigned long arg);
#else 
static long my_ioctl(struct file *f,unsigned int cmd,unsigned long arg);
#endif

//---------------------------------------------------------------

typedef struct
{
    int associate_pid;
    struct list_head list;
    struct list_head buffer;
} buffer_node_t;

struct list_head ma_list;

//----------------------------------------------------------------
/**
 *  Buffer  
 *  Noeud de buffer
 * */
typedef struct
{
    char *data;
    int size, cursor;
    struct list_head list;
} data_node_t;

struct list_head *current_list = NULL;

int init_flag = 0;

//---------------------------------------------------------------------

static dev_t dev;

static struct cdev *my_cdev;

static struct file_operations my_fops = {
    .owner = THIS_MODULE,
    .open = my_open,
    .release = my_release,
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
    .ioctl = my_ioctl
#else
    .unlocked_ioctl = my_ioctl
#endif 
};

static struct class *cl = NULL;

static data_node_t *current_dnt_read = NULL;
static int current_data_pos = 0;

//---------------------------------------------------------------

static DEFINE_MUTEX(my_mutex);

//---------------------------------------------------------------

static void list_destroy(void)
{
    buffer_node_t *pos, *next;
    data_node_t *in_pos, *in_next;
    printk(KERN_ALERT "[ Driver action ] free data buffer state \n");
    list_for_each_entry_safe(pos, next, &ma_list, list)
    {
        list_for_each_entry_safe(in_pos, in_next, &pos->buffer, list)
        {
            list_del(&in_pos->list);
            kfree(in_pos->data);
            kfree(in_pos);
        }
        list_del(&pos->list);
        kfree(pos);
    }
}

//----------------------------------------------------------------

static ssize_t my_read(struct file *f, char *buf, size_t s, loff_t *t)
{
    data_node_t *dnt;
    int size_to_copy, data_in_node, rr = 0;
    printk(KERN_ALERT "[ Driver action ] read of %ld octet \n", s);
    if (s > 0)
    {
        if (!list_empty(current_list) && current_dnt_read != NULL)
        {
            dnt = current_dnt_read;
            data_in_node = dnt->size - current_data_pos;
            size_to_copy = MIN(s, data_in_node);
            if ((rr = copy_to_user(buf, dnt->data + current_data_pos, size_to_copy)) == 0)
            {
                if (size_to_copy == data_in_node)
                {
                    if ((dnt->list.next) == current_list)
                    {
                        current_dnt_read = NULL;
                        current_data_pos = 0;
                    }
                    else
                    {
                        current_dnt_read = list_first_entry(&dnt->list, data_node_t, list);
                        current_data_pos = 0;
                    }
                }
                else
                {
                    current_data_pos = current_data_pos + size_to_copy;
                }
                printk(KERN_ALERT "[ Driver action ] read of %d octet\n", size_to_copy);
                return size_to_copy;
            }
            else
            {
                return -EFAULT;
            }
        }
    }
    return 0;
}

static ssize_t my_read_destroy(struct file *f, char *buf, size_t s, loff_t *t)
{
    data_node_t *dnt;
    int size_to_copy, data_in_node, rr = 0;
    printk(KERN_ALERT "[ Driver action ] read of %ld octet \n", s);
    if (s > 0 && !list_empty(current_list) && current_dnt_read != NULL)
    {
        dnt = current_dnt_read;
        data_in_node = dnt->size - current_data_pos;
        size_to_copy = MIN(s, data_in_node);
        if ((rr = copy_to_user(buf, dnt->data + current_data_pos, size_to_copy)) == 0)
        {
            dnt->cursor = dnt->cursor + size_to_copy;
            if (size_to_copy == data_in_node)
            {
                if ((dnt->list.next) == current_list)
                {
                    current_dnt_read = NULL;
                    current_data_pos = 0;
                }
                else
                {
                    current_dnt_read = list_first_entry(&dnt->list, data_node_t, list);
                    current_data_pos = 0;
                }
                list_del(&dnt->list);
                kfree(dnt->data);
                kfree(dnt);
            }
            else
            {
                current_data_pos = dnt->cursor;
            }
            printk(KERN_ALERT "[ Driver action ] read of %d octet\n", size_to_copy);
            return size_to_copy;
        }
        else
        {
            return -EFAULT;
        }
    }
    return 0;
}

static ssize_t my_write(struct file *f, const char *buf, size_t s, loff_t *t)
{
    printk(KERN_ALERT "[ Driver action ] write of %ld octet \n", s);
    if (s > 0)
    {
        data_node_t *dnt = kmalloc(sizeof(data_node_t), GFP_KERNEL);
        if (dnt)
        {
            dnt->data = kmalloc(sizeof(char) * s, GFP_KERNEL);
            if (dnt->data)
            {
                if (copy_from_user(dnt->data, buf, s) == 0)
                {
                    dnt->size = s;
                    dnt->cursor = 0;
                    INIT_LIST_HEAD(&(dnt->list));
                    list_add_tail(&(dnt->list), current_list);
                    return dnt->size;
                }
            }
            kfree(dnt);
        }
        return -EINVAL;
    }
    return s;
}




static struct list_head *get_pid_list(int pid)
{
    buffer_node_t *node;
    struct list_head *pid_list;
    pid_list = NULL;
    list_for_each_entry(node, &ma_list, list)
    {
        if (node->associate_pid == pid)
        {
            pid_list = &node->buffer;
        }
    }
    return pid_list;
}

static struct list_head *alloc_pid_list(int pid)
{
    buffer_node_t *bnt;
    bnt = kmalloc(sizeof(buffer_node_t), GFP_KERNEL);
    if (bnt)
    {

        bnt->associate_pid = pid;
        INIT_LIST_HEAD(&(bnt->list));
        INIT_LIST_HEAD(&(bnt->buffer));
        list_add_tail(&(bnt->list), &ma_list);
        return &bnt->buffer;
    }
    else
        return NULL;
}

static buffer_node_t *get_buffer_node_list(int pid)
{
    buffer_node_t *node;
    buffer_node_t *pid_list;
    pid_list = NULL;
    list_for_each_entry(node, &ma_list, list)
    {
        if (node->associate_pid == pid)
        {
            pid_list = node;
        }
    }
    return pid_list;
}

#define SAMPLE_IOC_MAGIC 'x'
#define SAMPLE_IOC_RESET _IO(SAMPLE_IOC_MAGIC, 0)
#define SAMPLE_IOC_MAXNR 0
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
static int my_ioctl(struct inode *i, struct file *f, unsigned int cmd, unsigned long arg)
#else 
static long my_ioctl(struct file *f,unsigned int cmd,unsigned long arg)
#endif
{
    if(_IOC_TYPE(cmd) != SAMPLE_IOC_MAGIC ) return -ENOTTY;
    if(_IOC_NR(cmd) > SAMPLE_IOC_MAXNR) return -ENOTTY;
    switch (cmd)
    {
    case SAMPLE_IOC_RESET:
        {
            int pid;
            buffer_node_t *cr_l;


            pid = current->pid;
            printk(KERN_ALERT "[IOCTL PID ] : %d \n", pid);
            if((cr_l = get_buffer_node_list(pid)) != NULL){
                data_node_t *in_pos, *in_next;
                printk(KERN_ALERT "[ Driver action ](ioctl) free buffer(@%p) of processus %d\n",cr_l, pid);
                list_for_each_entry_safe(in_pos, in_next, &cr_l->buffer, list)
                {
                    list_del(&in_pos->list);
                    kfree(in_pos->data);
                    kfree(in_pos);
                }
                list_del(&cr_l->list);
                kfree(cr_l);
                break;
            }
        }
    default:
        return -ENOTTY;
    }
    return 0;
}

static int my_open(struct inode *i, struct file *f)
{
    
    dev_t current_dev;
    int pid;
    struct list_head *cr_l;

    mutex_lock(&my_mutex);

    current_dev = i->i_rdev;

    pid = current->pid;

    printk(KERN_ALERT "[OPEN PID ] : %d \n", pid);

    cr_l = get_pid_list(pid);

    if (cr_l)
    {
        current_list = cr_l;
    }
    else
    {
        current_list = alloc_pid_list(pid);
    }

    if ( MINOR(current_dev) == MINOR(dev))
    {
        my_fops.write = my_write;
    }
    else if ((MINOR(current_dev) == MINOR(dev) + 2 || MINOR(current_dev) == MINOR(dev) + 1) )
    {
        if (current_dnt_read == NULL && !list_empty(current_list))
            current_dnt_read = list_first_entry(current_list, data_node_t, list);
        if (MINOR(current_dev) == MINOR(dev) + 2)
        {
            my_fops.read = my_read_destroy;
        }
        else
        {
            my_fops.read = my_read;
        }
    }

    return 0;
}

static int my_release(struct inode *i, struct file *f)
{
    my_fops.write = NULL;
    my_fops.read = NULL;
    mutex_unlock(&my_mutex);
    return 0;
}


//----------------------------------------------------------------------
static int buff_init(void)
{
    if (alloc_chrdev_region(&dev, 0, 3, DEVICE) == -1)
    {
        printk(KERN_ALERT ">>> ERROR alloc_chrdev_region\n");
        return -EINVAL;
    }
    printk(KERN_ALERT "[ Init allocated ] (major, minor)=(%d,%d)\n", MAJOR(dev), MINOR(dev));
    printk(KERN_ALERT "[ Driver State ] starting\n");

    my_cdev = cdev_alloc();
    my_cdev->ops = &my_fops;
    my_cdev->owner = THIS_MODULE;

    cl = class_create(THIS_MODULE, "chrdev");

    if (cl == NULL)
    {
        printk(KERN_ALERT ">>> ERROR : Class creation failed\n");
        unregister_chrdev_region(dev, 1);
        return -EINVAL;
    }

    if (device_create(cl, NULL, MKDEV(MAJOR(dev), MINOR(dev) + 0), NULL, "sample-write") == NULL)
    {
        printk(KERN_ALERT ">>> ERROR : Device creation failed\n");
        class_destroy(cl);
        unregister_chrdev_region(MKDEV(MAJOR(dev), 0), 1);
        return -EINVAL;
    }

    if (device_create(cl, NULL, MKDEV(MAJOR(dev), MINOR(dev) + 1), NULL, "sample-read") == NULL)
    {
        printk(KERN_ALERT ">>> ERROR : Device creation failed\n");
        class_destroy(cl);
        unregister_chrdev_region(MKDEV(MAJOR(dev), 0), 2);
        return -EINVAL;
    }

    if (device_create(cl, NULL, MKDEV(MAJOR(dev), MINOR(dev) + 2), NULL, "sample-read-destroy") == NULL)
    {
        printk(KERN_ALERT ">>> ERROR : Device creation failed\n");
        class_destroy(cl);
        unregister_chrdev_region(MKDEV(MAJOR(dev), 0), 3);
        return -EINVAL;
    }

    if (cdev_add(my_cdev, MKDEV(MAJOR(dev), 0), 3) <= -1)
    {
        device_destroy(cl, dev);
        class_destroy(cl);
        unregister_chrdev_region(MKDEV(MAJOR(dev), 0), 3);
        return -EINVAL;
    }

    INIT_LIST_HEAD(&ma_list);

    return 0;
}

static void buff_cleanup(void)
{
    printk(KERN_ALERT "[ Driver State ] stopping\n");
    cdev_del(my_cdev);
    device_destroy(cl, MKDEV(MAJOR(dev), MINOR(dev) + 0));
    device_destroy(cl, MKDEV(MAJOR(dev), MINOR(dev) + 1));
    device_destroy(cl, MKDEV(MAJOR(dev), MINOR(dev) + 2));
    class_destroy(cl);
    unregister_chrdev_region(MKDEV(MAJOR(dev), 0), 3);
    list_destroy();
}

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTEUR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);

module_init(buff_init);
module_exit(buff_cleanup);
