#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/ioctl.h>

#define SAMPLE_IOC_MAGIC 'x'
#define SAMPLE_IOC_RESET _IO(SAMPLE_IOC_MAGIC, 0)

char *message = "Hello World !";

void write_message(int f)
{
    if (write(f, message, 14) > 0)
    {
        fprintf(stdout, "write ok\n");
    }
    else
    {
        fprintf(stdout, "write not ok\n");
    }
}

int main(int argc, char **argv)
{
    int f;
    if((f = open("/dev/sample-write", O_RDWR)) == -1){
        printf("cannot open the file\n");
        return EXIT_FAILURE;
    }
    printf("write message \n");
    write_message(f);
    ioctl(f,SAMPLE_IOC_RESET,0);
    close(f);
    return EXIT_SUCCESS;
}